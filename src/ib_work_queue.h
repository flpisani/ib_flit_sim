//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef __IB_WORK_QUEUE_H
#define __IB_WORK_QUEUE_H

#include <omnetpp.h>
#include "ib_m.h"
#include "ib_data_msg.h"
#include "vlarb.h"
#include <vec_file.h>
#include <queue>
#include <fstream>
#include <better_histo.h>
#include <vector>
#include <cstring>
#include <cerrno>

class IBWorkQueue : public cSimpleModule
{
 protected:
  std::vector<bool> app_sending;
  std::vector<std::queue <IBAppMsg*> > send_msg_queue;

  // Initialize a new set of parameters for a new message
  IBAppMsg *getNewMsg();
  std::vector<cGate *> out;
  std::vector<cGate *> in;
  std::vector<cDelayChannel *> output_channel;

  // parameters
  int num_qp;
  double pci_latency_mean;
  double pci_latency_sigma;

  bool use_random_distribution;

  Better_histo pci_latency_histo;

	// adding static variables to speed up the network generation
	// TODO check if there is a better solution
  static Better_histo static_pci_latency_histo;
  static std::string static_pci_latency_distribution_file_name;
 public:

 protected:
  virtual void initialize();
  virtual void finish();
  virtual void handleMessage(cMessage* msg);
  void handleOut(IBAppMsg* msg, int gate_idx);
  void handleIn(IBAppMsg *p_msg, int gate_idx);
  void set_latency_histo(const std::string &file_name);
  void update_delay(int gate_idx);

};

#endif
