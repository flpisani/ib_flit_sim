//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef IB_DATA_MSG_H
#define IB_DATA_MSG_H 1

#include "ib_m.h"

class IBDataMsg : public IBDataMsg_Base
{
  public:
    IBDataMsg(const char *name=NULL, int kind=0) : IBDataMsg_Base(name, kind) {}
    IBDataMsg(const IBDataMsg& other) : IBDataMsg_Base(other) {}
    IBDataMsg& operator=(const IBAppMsg& other);
    IBDataMsg& operator=(const IBDataMsg& other)
        {IBDataMsg::operator=(other); return *this;}
    virtual IBDataMsg *dup() const {return new IBDataMsg(*this);}

		bool is_last_flit();

};

#endif // IB_DATA_MSG_H
