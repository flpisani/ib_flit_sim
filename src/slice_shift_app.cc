//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include <slice_shift_app.h>

Define_Module(SliceShiftApp);

Json::Value SliceShiftApp::config_root;
std::string SliceShiftApp::config_file_name;

SliceShiftApp::SliceShiftApp()
{
  // TODO Auto-generated constructor stub

}

SliceShiftApp::~SliceShiftApp()
{
  // TODO Auto-generated destructor stub
}

void SliceShiftApp::initialize()
{
  GenericIBApp::initialize();

  parse_config_file();

  window_time = par("window_time");
  slice_time = par("slice_time");

  fragment_size = par("fragment_size");

  // inverse of the bw in s/B
  byte_time_slot = 8/(check_and_cast<cDatarateChannel*>(getParentModule()->gate("port$o")->getTransmissionChannel())->getDatarate());

  // init the time window
  window_start = simTime();
  window_end = window_start + window_time;

  auto find_bu = std::find(bu_lids.begin(), bu_lids.end(), srcLid);

  out_of_window = registerSignal("out_of_window");

  if (find_bu != bu_lids.end()){
    my_rank = find_bu - bu_lids.begin();
    curr_rank = my_rank;
    update_destination();
    send_and_update();
  }

  if (num_qp < bu_lids.size()){
    opp_error("-E- %s not enough QPs. QP instantiated=%d QP needed=%d",
        getFullPath().c_str(), num_qp, bu_lids.size());
  }
}

void SliceShiftApp::handleOut(IBAppMsg* p_msg)
{
  send_and_update();
}

void SliceShiftApp::handleSelf(cMessage* msg)
{
  send_and_update();
}

void SliceShiftApp::finish()
{
  GenericIBApp::finish();
}

void SliceShiftApp::update_window()
{
  window_start += window_time;
  window_end += window_time;
}

void SliceShiftApp::send_and_update()
{
  update_window_status();
  if (status == IN_SEND){
    sendOutMsg(bu_lids[curr_rank], fragment_size);
  } else {
    update_destination();
    update_window();
    if (status == IN_IDLE){
      scheduleAt(window_start, new(cMessage));
    } else {
      emit(out_of_window,1);
      sendOutMsg(bu_lids[curr_rank], fragment_size);
    }
  }
}


void SliceShiftApp::parse_config_file()
{
  std::string file_name(par("config_file_name").stdstringValue());
  Json::Value tmp_value;

  int num_active_bus = par("num_active_bus");

  if (file_name != config_file_name){
      update_static_config_file(file_name);
  }

  // setting up bu lids
  tmp_value = config_root["bu_lids"];

  if (!tmp_value.isArray()){
    opp_error("-E- %s bad config file %s is not a vector", getFullPath().c_str(),
        "bu_lids");
  }

  for (auto const & elem : tmp_value){
    bu_lids.push_back(elem.asInt());
  }

  if (bu_lids.size() >= num_active_bus){
    bu_lids.resize(num_active_bus);
  } else {
    opp_warning("-W- %s %d BUs requested with only %d available ignoring number of BUs",
        getFullPath().c_str(), num_active_bus, bu_lids.size());
  }

}

void SliceShiftApp::update_window_status()
{
  simtime_t next_send_time = simTime() + fragment_size*byte_time_slot;
  if (next_send_time <= (window_end - slice_time)){
    status = IN_SEND;
    EV << "status IN_SEND" << std::endl;
  } else if (next_send_time <= window_end) {
    status = IN_IDLE;
    EV << "status IN_IDLE" << std::endl;
  } else {
    status = OUT;
    EV << "status OUT" << std::endl;
  }
}

void SliceShiftApp::update_destination()
{
  curr_rank = (curr_rank + 1) % bu_lids.size();
}

void SliceShiftApp::update_static_config_file(std::string file_name) {
    config_file_name = file_name;

    std::cout << getFullPath() << " updating config file " << file_name << std::endl;

    std::ifstream config_file(config_file_name);
    if (!config_file.is_open()){
        opp_error("-E- %s unable to open config file %s", getFullPath().c_str(),
                config_file_name.c_str());
    }
    config_file >> config_root;
}
