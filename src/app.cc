///////////////////////////////////////////////////////////////////////////
//
//         InfiniBand FLIT (Credit) Level OMNet++ Simulation Model
//
// Copyright (c) 2004-2013 Mellanox Technologies, Ltd. All rights reserved.
// This software is available to you under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////
//
// IB App Message Generator:
//
// Internal Messages:
// None
//
// External Messages:
// done/any - IN: the last provided appMsg was consumed - gen a new one
// IBAppMsg - OUT: send the message to the gen
//
// For a full description read the app.h
//

#include "app.h"

using namespace std;

Define_Module(BasePatternIBApp);

void BasePatternIBApp::parseIntListParam(const char *parName, std::vector<int> &out)
{
  int cnt = 0;
  const char *str = par(parName);
  char *tmpBuf = new char[strlen(str)+1];
  strcpy(tmpBuf, str);
  char *entStr = strtok(tmpBuf, " ,");
  if (out.size()) out.clear();
  while (entStr) {
    cnt++;
    out.push_back(atoi(entStr));
    entStr = strtok(NULL, " ,");
  }
}

// main init of the module
void BasePatternIBApp::initialize(){
  GenericIBApp::initialize();
  // init destination sequence related params
  dstSeqIdx = 0;
  dstSeqDone = 0;
  msgIdx = 0;

  //  destination mode
  const char *dstModePar = par("dstMode");
  if (!strcmp(dstModePar, "param")) {
    msgDstMode = DST_PARAM;
  } else if (!strcmp(dstModePar, "seq_once")) {
    msgDstMode = DST_SEQ_ONCE;
  } else if (!strcmp(dstModePar, "seq_loop")) {
    msgDstMode = DST_SEQ_LOOP;
  } else if (!strcmp(dstModePar, "seq_rand")) {
    msgDstMode = DST_SEQ_RAND;
  } else {
    error("unknown dstMode: %s", dstModePar);
  }

  // destination related parameters
  if (msgDstMode != DST_PARAM) {
    const char *dstSeqVecFile = par("dstSeqVecFile");
    int   dstSeqVecIdx  = par("dstSeqVecIdx");
    if(dstSeqVecIdx == -1){
        if(getParentModule()->isVector()){
            dstSeqVecIdx = getParentModule()->getIndex();
        } else {
            opp_error("-E- %s got dstSeqVector=-1 but is not part of a vector aborting", getFullPath().c_str());
        }
    }

    vecFiles   *vecMgr = vecFiles::get();
    dstSeq = vecMgr->getIntVec(dstSeqVecFile, dstSeqVecIdx);
    if (dstSeq == NULL) {
            opp_error("fail to obtain dstSeq vector: %s/%d",
                               dstSeqVecFile, dstSeqVecIdx);
    }
    EV << "-I- Defined DST sequence of " << dstSeq->size() << " LIDs" << endl;
  }

  // Message Length Modes
  const char *msgLenModePar = par("msgLenMode");
  if (!strcmp(msgLenModePar,"param")) {
    msgLenMode = MSG_LEN_PARAM;
  } else if (!strcmp(msgLenModePar,"set")) {
    msgLenMode = MSG_LEN_SET;
  } else {
    opp_error("unknown msgLenMode: %s", msgLenMode);
  }

  // need to init the set...
  if (msgLenMode == MSG_LEN_SET) {
    parseIntListParam("msgLenSet", msgLenSet);
    vector<int> msgLenProbVec;
    parseIntListParam("msgLenProb", msgLenProbVec);

    if (msgLenSet.size() != msgLenProbVec.size()) {
      error("provided msgLenSet size: %d != msgLenProb size: %d",
            msgLenSet.size(), msgLenProbVec.size());
    }

    // convert the given probabilities into a histogram
    // with Prob[idx] where idx is the index of the length in the vector
    msgLenProb.setNumCells(msgLenSet.size());
    msgLenProb.setRange(0,msgLenSet.size()-1);
    // HACK: there must be a faster way to do this!
    for (unsigned int i = 0; i < msgLenProbVec.size(); i++)
      for (int p = 0; p < msgLenProbVec[i]; p++)
        msgLenProb.collect(i);

    EV << "-I- Defined Length Set of " << msgLenSet.size() << " size" << endl;
  }

  seqIdxVec.setName("Dst-Sequence-Index");

  // if we are in param mode we may be getting a 0 as DST and thus keep quite
  if (msgDstMode == DST_PARAM) {
      int dstLid = par("dstLid");
      if (dstLid)
          scheduleAt(simTime(), new cMessage);
  } else if (dstSeq->size() != 1 || dstSeq->front() != 0) {
    // this node is disabled there is on one destination ad it's 0
    scheduleAt(simTime(), new cMessage);
  }
}

// get random msg len by the histogram
unsigned int BasePatternIBApp::getMsgLenByDistribution()
{
    double r = msgLenProb.random();
    return int(r);
}

void BasePatternIBApp::handleOut(IBAppMsg *p_msg){
  if (!dstSeqDone) {
    // generate a new messaeg and send after hiccup
    simtime_t delay = par("msg2msgGap");
    sendOutMsg(calc_next_dst(), calc_next_size(), delay, calc_next_sq());
  }
}

unsigned int BasePatternIBApp::calc_next_size()
{
  unsigned int msgLen_B;
  // obtain the message length
  switch (msgLenMode) {
  case MSG_LEN_PARAM:
    msgLen_B = par("msgLength");
    break;
  case MSG_LEN_SET:
    msgLen_B = getMsgLenByDistribution();
    break;
  default:
    error("unsupported msgLenMode: %d", msgLenMode);
    break;
  }

  return msgLen_B;
}

unsigned int BasePatternIBApp::calc_next_sq()
{
  return par("msgSQ");

}

unsigned int BasePatternIBApp::calc_next_dst()
{
  // obtain the message destination
  unsigned int msgDstLid;
  switch (msgDstMode) {
  case DST_PARAM:
    msgDstLid = par("dstLid");
    break;
  case DST_SEQ_ONCE:
    msgDstLid = (*dstSeq)[dstSeqIdx++];
    if (dstSeqIdx == dstSeq->size()) {
            dstSeqDone = 1;
    }
//       seqIdxVec.record(dstSeqIdx);
    break;
  case DST_SEQ_LOOP:
    msgDstLid = (*dstSeq)[dstSeqIdx++];
    if (dstSeqIdx == dstSeq->size()) {
            dstSeqIdx = 0;
    }
//       seqIdxVec.record(dstSeqIdx);
    break;
  case DST_SEQ_RAND:
    dstSeqIdx = intuniform(0,dstSeq->size()-1);
    msgDstLid = (*dstSeq)[dstSeqIdx];
    break;
  default:
    error("unsupported msgDstMode: %d", msgDstMode);
    break;
  }

  return msgDstLid;
}

void BasePatternIBApp::handleSelf(cMessage* msg)
{
  handleOut(NULL);
}

void BasePatternIBApp::finish()
{
}
