///////////////////////////////////////////////////////////////////////////
//
//         InfiniBand FLIT (Credit) Level OMNet++ Simulation Model
//
// Copyright (c) 2004-2013 Mellanox Technologies, Ltd. All rights reserved.
// This software is available to you under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////
//
// IB App Message Generator:
//
// Internal Messages:
// None
//
// External Messages:
// done/any - IN: the last provided appMsg was consumed - gen a new one
// IBAppMsg - OUT: send the message to the gen
//
// For a full description read the app.h
//

#include "generic_app.h"

using namespace std;

std::vector<cGate *> GenericIBApp::ideal_gates;

// main init of the module
void GenericIBApp::initialize(){
  // init destination sequence related params
  msgIdx = 0;
  msgMtuLen_B = par("msgMtuLen");

  appIdx = getIndex();

  num_qp = par("num_qp");

  srcLid = par("srcLid");
  if(srcLid == -1){
      if(getParentModule()->isVector()){
        srcLid = getParentModule()->getIndex() + 1;
      } else {
        opp_error("-E- %s got srcLid=-1 but is not part of a vector aborting", getFullPath().c_str());
      }
  }

  if(ideal_gates.size() <= srcLid) {
      ideal_gates.resize(srcLid+1, NULL);
  }

  ideal_gates[srcLid] = gate("ideal_port");

  sink_ack = gate("sink_ack");
}


// Initialize the parameters for a new message by sampling the
// relevant parameters and  allocate and init a new message
void GenericIBApp::sendOutMsg(unsigned int msgDstLid,
       unsigned int msgLen_B, simtime_t delay ,
       unsigned int msgSQ , short app_kind , int userData ,
       bool isIdeal , unsigned int msgWQ, int flitAccuracy)
{

  unsigned int msgLen_P;    // the message length in packets

  msgLen_P = (msgLen_B + msgMtuLen_B - 1)/ msgMtuLen_B;


  IBAppMsg *p_msg;
  char name[128];
  // sprintf(name, "app-%s-%d", getFullPath().c_str() ,msgIdx);
  sprintf(name, "app-%s-%d", "pippo" ,msgIdx);
  p_msg = new IBAppMsg(name, (app_kind & IB_APP_MASK) | IB_APP_MSG);
  p_msg->setAppIdx(appIdx);
  p_msg->setMsgIdx(msgIdx);
  p_msg->setDstLid(msgDstLid);
  p_msg->setSQ(msgSQ);
  p_msg->setLenBytes(msgLen_B);
  p_msg->setLenPkts(msgLen_P);
  p_msg->setMtuBytes(msgMtuLen_B);
  p_msg->setUserData(userData);
  p_msg->setSrcLid(srcLid);
  p_msg->setFlitAccuracy(flitAccuracy);
  msgIdx++;

  if(isIdeal && (ideal_gates[msgDstLid] != NULL)){
      sendDirect(p_msg, ideal_gates[msgDstLid]);
  } else {
      sendDelayed(p_msg, delay, "out$o", msgWQ);
  }

}

void GenericIBApp::finish()
{
}

void GenericIBApp::handleMessage(cMessage* msg)
{
    if (msg->getArrivalGate() == sink_ack){
        IBDataMsg* p_msg = check_and_cast<IBDataMsg*>(msg);
        handleAck(p_msg);
    } else if (msg->getArrivalGate() == ideal_gates[srcLid]){
        IBAppMsg* p_msg = check_and_cast<IBAppMsg*>(msg);
        IBDataMsg *p_data_msg = new IBDataMsg();
        *p_data_msg = *p_msg;
        handleAck(p_data_msg);
        delete p_data_msg;
    } else if (msg->isSelfMessage()){
        handleSelf(msg);
    } else {
        IBAppMsg* p_msg = check_and_cast<IBAppMsg*>(msg);
        handleOut(p_msg);
    }

    delete msg;
}
