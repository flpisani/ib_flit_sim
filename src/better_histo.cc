//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.

#include <better_histo.h>

Better_histo::Better_histo()
{
  // TODO Auto-generated constructor stub

}

Better_histo::~Better_histo()
{
  // TODO Auto-generated destructor stub
}

void Better_histo::collect_weighted(double value, int weight)
{
  for (int k = 0 ; k<weight ; k++){
    this->collect(value);
  }
}
