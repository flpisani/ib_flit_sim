//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "ib_work_queue.h"

Define_Module(IBWorkQueue);

Better_histo IBWorkQueue::static_pci_latency_histo;
std::string  IBWorkQueue::static_pci_latency_distribution_file_name;

// main init of the module
void IBWorkQueue::initialize(){
  // init destination sequence related params

  num_qp = par("num_qp");

  send_msg_queue.resize(num_qp);
  app_sending.resize(num_qp,false);

  out.resize(num_qp);
  in.resize(num_qp);
  output_channel.resize(num_qp);

  for (int i = 0 ; i<num_qp ; i++){
	  out[i] = gate("out$i", i);
	  in[i] = gate("in$i", i);
	  output_channel[i] = check_and_cast<cDelayChannel*>(gate("out$o", i)->getChannel());
  }

  pci_latency_mean = par("pci_latency_mean");
  pci_latency_sigma = par("pci_latency_sigma");

  std::string pci_latency_distribution_file = par("pci_latency_distribution_file");
  if(pci_latency_distribution_file != "None"){
    use_random_distribution = true;
		if(pci_latency_distribution_file != static_pci_latency_distribution_file_name){
			set_latency_histo(pci_latency_distribution_file);
		}
		pci_latency_histo = static_pci_latency_histo;
  }else{
    use_random_distribution = false;
  }
}

void IBWorkQueue::finish()
{
}

void IBWorkQueue::handleMessage(cMessage* msg)
{
  cGate *arrival_gate = msg->getArrivalGate();
  int gate_idx = arrival_gate->getIndex();
  if (arrival_gate == out[gate_idx]){
    IBAppMsg* p_msg = check_and_cast<IBAppMsg*>(msg);
    handleOut(p_msg, gate_idx);
  } else if (arrival_gate == in[gate_idx]){
    IBAppMsg* p_msg = check_and_cast<IBAppMsg*>(msg);
    handleIn(p_msg, gate_idx);
  } else {
    opp_error("-E- %s unexpected message aborting", getFullPath().c_str());
  }
}

void IBWorkQueue::handleOut(IBAppMsg* msg, int gate_idx)
{
  // send next msg in queue or enter into idle state
  if (send_msg_queue[gate_idx].size() != 0){
    update_delay(gate_idx);
    // std::cout << "qp channel delay " << output_channel->getDelay() << std::endl;
    send(send_msg_queue[gate_idx].front(), "out$o", gate_idx);
    send_msg_queue[gate_idx].pop();
    app_sending[gate_idx] = true;
  } else {
    app_sending[gate_idx] = false;
  }
  send(msg, "in$o", gate_idx);
}

void IBWorkQueue::handleIn(IBAppMsg* p_msg, int gate_idx)
{
  if (app_sending[gate_idx]){
    send_msg_queue[gate_idx].push(p_msg);
  } else {
    app_sending[gate_idx] = true;
    update_delay(gate_idx);
    // std::cout << "qp channel delay " << output_channel->getDelay() << std::endl;
    send(p_msg, "out$o", gate_idx);
  }
}

void IBWorkQueue::set_latency_histo(const std::string &file_name)
{
// TODO add file syntax check

	static_pci_latency_distribution_file_name = file_name;

  std::ifstream input_file(static_pci_latency_distribution_file_name);

  if (!input_file.is_open()){
      opp_error("-E- unable to open pci latency distribution file %s: %s", file_name.c_str(), std::strerror(errno));
  }

  std::vector<double> bin_low;
  std::vector<int> bin_content;
  double tmp_bin, tmp_content;
  while(input_file >> tmp_bin >> tmp_content){
    bin_low.push_back(tmp_bin);
    bin_content.push_back(tmp_content);
  }

  static_pci_latency_histo.setNumCells(bin_low.size());
  static_pci_latency_histo.setRange(bin_low[0], bin_low[1]-bin_low[0]+bin_low[bin_low.size()-1]);
  static_pci_latency_histo.setNumFirstVals(bin_low.size());
  for (int i = 0 ; i<bin_low.size() ; i++){
    static_pci_latency_histo.collect_weighted(bin_low[i],bin_content[i]);
  }
}

void IBWorkQueue::update_delay(int gate_idx)
{
  double new_delay ;
  if(use_random_distribution){
    new_delay = pci_latency_histo.random()*1e-6;
  }else{
    new_delay = fabs(normal(pci_latency_mean, pci_latency_sigma));
  }
  output_channel[gate_idx]->setDelay(new_delay);
}
