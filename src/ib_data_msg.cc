//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "ib_data_msg.h"


IBDataMsg& IBDataMsg::operator=(const IBAppMsg& other){
    cMessage::operator= (other);
    this->setAppIdx(other.getAppIdx());
    this->setMsgIdx(other.getMsgIdx());
    this->setDstLid(other.getDstLid());
    this->setSrcLid(other.getSrcLid());
    this->setPacketLength(other.getLenBytes());
    this->setUserData(other.getUserData());
    this->setMsgLen(1);
    this->setPktIdx(0);

    return *this;
}
bool IBDataMsg::is_last_flit(){
	return (getFlitSn() + getFlitAccuracy() == getPacketLength());
}
