//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef EB_APP_H_
#define EB_APP_H_

#include <generic_app.h>
#include <cmath>
#include <string>
#include <queue>
#include <vector>
#include <algorithm>
#include <fstream>
#include "../json_parser/json/json.h"
#include <cmath>
#include <random>

class EbApp : public GenericIBApp
{
  public:
    EbApp();
    virtual ~EbApp();
  protected:
    virtual void initialize();
    virtual void handleAck(IBDataMsg *p_msg);
    virtual void handleSelf(cMessage *msg);
    virtual void handleOut(IBAppMsg* msg);
    virtual void finish();

    void send_ru_requests();
    void send_next_ru_request(int credit);
    void send_em_request();
    void send_event_fragment(IBDataMsg *p_msg);
    void send_event_assign(IBDataMsg *p_msg);
    void handle_event_fragment(IBDataMsg *p_msg);
    void handle_end(IBDataMsg *p_msg);
    void send_end();

    void parse_config_file();
    void extract_vector(std::vector<int> &lid_to_idx, std::vector<int> &idx_to_lid,
        std::string key);

    void check_flit_warmup(); // check if the warmup is over and update the flit accuracy and the warmup flag


    // TODO add support for different number of fragment
    std::vector<int> fragment_received;
    std::vector<std::vector <int> > pending_fragments;
    int num_credits;

    int data_size;
    int metadata_size;
    int em_event_assign_size;
    int em_request_size;
    int ru_request_size;

    int number_of_rails;

    std::vector<int> curr_send_idx;
    int max_parallel_sends;
    std::vector<int> num_missing_sends;

    int max_built;
    bool is_max_built_active;

    int max_assigned;
    bool is_max_assigned_active;

    int built_events;
    int assigned_events;

    Json::Value config_root;

    // LID assignments
    int event_manager_lid;
    int my_rank;

    std::vector<bool> active_bu;
    int num_active_bu;

    std::vector<int> ru_idx_to_lids;
    std::vector<int> bu_idx_to_lids;

    std::vector<int> ru_lid_to_idx;
    std::vector<int> bu_lid_to_idx;

    std::queue <IBDataMsg*> received_msg_queue;

    // BW limiter this will reduce the size of the event fragments

    int size_fraction; // max fragment size fraction in %

    static std::vector<int> fraction_distribution;

    enum app_msg_type{
      // max 7 values
      EM_REQUEST = IB_MSG_MASK + 1,
      EM_EVENT_ASSIGN = EM_REQUEST + IB_MSG_MASK + 1,
      RU_REQUEST = EM_EVENT_ASSIGN + IB_MSG_MASK + 1,
      DATA = RU_REQUEST + IB_MSG_MASK + 1,
      BU_END = DATA + IB_MSG_MASK + 1
    };

    unsigned int event_size;

    simsignal_t sig_ru_sent_bytes;
    simsignal_t sig_built_events;
    simsignal_t sig_assigned_events;

    // ideal em i.e. credit announces don't use IB
    bool is_ideal_em;

    int flit_accuracy;
    int run_flit_accuracy;
    int warmup_flit_accuracy;
    simtime_t warmup_time;
    bool warmup_over;
};

#endif /* EB_APP_H_ */
