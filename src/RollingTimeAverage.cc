//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "RollingTimeAverage.h"

Register_ResultRecorder("rollingTimeAverage", RollingTimeAverage);

RollingTimeAverage::RollingTimeAverage() {

}

void RollingTimeAverage::collect(simtime_t_cref t, double value) {
    bool record = false;

    sum += value;

    _values.push_back(std::make_pair(t, value));

    while(_values.front().first <= t - _average_window){
        sum -= _values.front().second;
        _values.pop_front();
    }

    if(t - _last_emission >= _emission_frequency){
        if(_values.back().first != _values.front().first){
            value = (sum /(_values.back().first - _values.front().first))/_normalizaion;
            record = true;
            _last_emission = t;
        }
    }

    if(record){
        MyVectorRecorder::collect(t,value);
    }
}

RollingTimeAverage::~RollingTimeAverage() {
    // TODO Auto-generated destructor stub

}
