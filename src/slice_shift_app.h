//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef SLICE_SHIFT_APP_H_
#define SLICE_SHIFT_APP_H_

#include <generic_app.h>
#include "../json_parser/json/json.h"
#include <vector>
#include <fstream>
#include <algorithm>

class SliceShiftApp : public GenericIBApp
{
  public:
    SliceShiftApp();
    virtual ~SliceShiftApp();
  protected:
    enum window_status {
      IN_SEND,
      IN_IDLE,
      OUT
    };

    virtual void initialize();
    virtual void handleOut(IBAppMsg *p_msg);
    virtual void handleSelf(cMessage *msg);
    virtual void finish();

    void update_window();
    void update_window_status();
    void send_and_update();
    void parse_config_file();
    void send_fragment();
    void update_destination();

    void update_static_config_file(std::string file_name);

    // param
    // time window param
    simtime_t window_time;
    simtime_t slice_time;

    int fragment_size;
    double byte_time_slot;

    std::vector <int> bu_lids;
    int my_rank;
    int curr_rank;

    // window time control
    simtime_t window_start;
    simtime_t window_end;
    simtime_t next_window_check;

    window_status status;

    simsignal_t out_of_window;

    // static variable for the configuration file
    // this variable is static to speed up the generation of the network
    static Json::Value config_root;
    static std::string config_file_name;
};

#endif /* SLICE_SHIFT_APP_H_ */
