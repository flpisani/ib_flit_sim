//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include <eb_app.h>

Define_Module(EbApp);

std::vector<int> EbApp::fraction_distribution;

EbApp::EbApp()
{
  // TODO Auto-generated constructor stub

}

EbApp::~EbApp()
{
  // TODO Auto-generated destructor stub
}

void EbApp::initialize()
{
  GenericIBApp::initialize();

  parse_config_file();

  // TODO put real sizes
  em_event_assign_size = par("em_event_assign_size");
  em_request_size = par("em_request_size");
  ru_request_size = par("ru_request_size");
  data_size = par("data_size");
  // default value from minidaqpipe the max size for a metadata is 1/100 of the data
  metadata_size = data_size / 100;
  num_credits = par("num_credits");

  number_of_rails = par("number_of_rails");

  is_ideal_em = par("ideal_em");

  run_flit_accuracy = par("run_flit_accuracy");
  warmup_flit_accuracy = par("warmup_flit_accuracy");
  warmup_time = par("warmup_time");

  // init variables and check for warmup_time
  flit_accuracy = warmup_flit_accuracy;
  warmup_over = false;
  check_flit_warmup();

  max_parallel_sends = par("max_parallel_sends");
  if(max_parallel_sends > ru_idx_to_lids.size()-1){
    max_parallel_sends = ru_idx_to_lids.size()-1;
    opp_warning("-W- max_parallel_sends is bigger than the number of RUs");
  }

  fragment_received.resize(num_credits,0);
  curr_send_idx.resize(num_credits,0);
  num_missing_sends.resize(num_credits,0);

  num_active_bu = bu_idx_to_lids.size();

  max_built = par("max_built");
  is_max_built_active = max_built > 0;

  max_assigned = par("max_assigned");
  is_max_assigned_active = max_assigned > 0;

  assigned_events = 0;
  built_events = 0;


// appling selected bw limit

  // if this node is a valid BU sends first request to EM and sets the rank

  if (srcLid < bu_lid_to_idx.size()){
    my_rank = bu_lid_to_idx[srcLid];
  } else {
    my_rank = -1;
  }

  // TODO this is just a test
  size_fraction = 10*(my_rank+1)%100;
  if (my_rank == 3){
      size_fraction = 100;
  }
  data_size = data_size*fraction_distribution[my_rank]/100.;

  recordScalar("fragment_size", data_size);

  std::cout << "rank " << my_rank << " data size " << data_size << " size fraction " << fraction_distribution[my_rank] << std::endl;

  if(my_rank != -1){
    for (int k = 0 ; k<num_credits ; k++){
      send_em_request();
    }

    pending_fragments.resize(num_credits, std::vector<int>(num_active_bu, 0));
  }

  if (num_qp < bu_idx_to_lids.size()) {
    opp_error("-E- %s not enough QPs. QP instantiated=%d QP needed=%d",
        getFullPath().c_str(), num_qp, bu_idx_to_lids.size());
  }else if (num_qp < ru_idx_to_lids.size()){
    opp_error("-E- %s not enough QPs. QP instantiated=%d QP needed=%d",
        getFullPath().c_str(), num_qp, ru_idx_to_lids.size());
  }

  if (srcLid == event_manager_lid) {
    active_bu.resize(num_active_bu, true);
  }

  // registering signals

  sig_ru_sent_bytes = registerSignal("ru_sent_bytes");
  sig_built_events = registerSignal("built_events");
  sig_assigned_events = registerSignal("assigned_events");

}

void EbApp::handleSelf(cMessage* msg)
{
}

void EbApp::check_flit_warmup(){
  if(!warmup_over){
    if(simTime() >= warmup_time){
      warmup_over = true;
      flit_accuracy = run_flit_accuracy;
    }
  }
}

void EbApp::handleAck(IBDataMsg* p_msg)
{
  // we care only about the last packet in the message
  if (p_msg->getMsgLen() == p_msg->getPktIdx()+1){

    // TODO we may implement something more object oriented
    short app_msg_kind = (p_msg->getKind() & IB_APP_MASK);

    check_flit_warmup();

    if (app_msg_kind == EM_EVENT_ASSIGN){
      send_ru_requests();
    } else if (app_msg_kind == RU_REQUEST){
      send_event_fragment(p_msg);
    } else if ((app_msg_kind == EM_REQUEST) && (event_manager_lid == srcLid)){
      send_event_assign(p_msg);
    } else if (app_msg_kind == DATA){
      handle_event_fragment(p_msg);
    } else if ((app_msg_kind == BU_END) && (event_manager_lid == srcLid)){
      handle_end(p_msg);
    } else {
      opp_warning(" -E- %s message of unknown kind %d received aborting...", getFullPath().c_str(), app_msg_kind);
    }
  }

}

void EbApp::finish()
{
  std::cout << "from lid " << srcLid << ": assigned events " << assigned_events << " received " << built_events << std::endl;

}

void EbApp::send_ru_requests()
{
  int active_credit = 0;
  auto fragment_it = fragment_received.begin();
  while (*fragment_it != 0 && fragment_it != fragment_received.end()){
    fragment_it++;
    active_credit++;
  }
  if (fragment_it == fragment_received.end()){
    opp_error("-E- %s Sending event assign to a BU with no available credits",
        getFullPath().c_str());
  }

  // TODO add support for different number of fragments per event

  curr_send_idx[active_credit] = my_rank;
  for (int k = 0 ; k < max_parallel_sends ; k++){
    send_next_ru_request(active_credit);
  }

  // We don't' send to ourself
  num_missing_sends[active_credit] = (ru_idx_to_lids.size() - 1) - max_parallel_sends;
  if(num_missing_sends[active_credit] < 0){
    opp_error("-E- BU negative number of missing fragments to many parallel sends %d",
        num_missing_sends[active_credit]);
  }
  // num_missing_sends[active_credit] = (ru_idx_to_lids.size() - 1)*2*number_of_rails - max_parallel_sends;

  fragment_received[active_credit] = 1;
}

void EbApp::send_event_fragment(IBDataMsg* p_msg)
{
  unsigned int msgDstLid = p_msg->getSrcLid();
  int userData = p_msg->getUserData();

  for (int k = 0 ; k < number_of_rails ; k++){
    // sending metadata
    sendOutMsg(msgDstLid, metadata_size, 0, 0, DATA, userData, false, msgDstLid-1, flit_accuracy);

    // sending data
    // msgLen_B = data_size*uniform(0.7,1);
    sendOutMsg(msgDstLid, data_size, 0, 0, DATA, userData, false, msgDstLid-1, flit_accuracy);
  }

  EV << "RU " << srcLid << " sending fragments to BU " << msgDstLid << std::endl;

}

void EbApp::send_event_assign(IBDataMsg* p_msg)
{
  if(assigned_events <= max_assigned || !is_max_assigned_active){
      assigned_events++;
      unsigned int msgDstLid = p_msg->getSrcLid();

      emit(sig_assigned_events, msgDstLid);
      sendOutMsg(msgDstLid, em_event_assign_size, 0, 0, EM_EVENT_ASSIGN, 0, is_ideal_em, msgDstLid-1, flit_accuracy);
      EV << "EM assigned event to " << msgDstLid << std::endl;
   } else {
       endSimulation();
   }
}

void EbApp::handle_event_fragment(IBDataMsg* p_msg)
{
  // we find the active credit

  int active_credit = p_msg->getUserData();
  int ru_lid = p_msg->getSrcLid();
  int ru_idx = ru_lid_to_idx[ru_lid];


  if (--pending_fragments[active_credit][ru_idx] == 0){
    fragment_received[active_credit]++;

    if (bu_idx_to_lids[0] == my_rank){
      std::cout << fragment_received[active_credit] << " " ;
    }

    EV << "BU " << srcLid << " fragment " << fragment_received[active_credit]
       << " rcv from " << p_msg->getSrcLid() << std::endl;

    if(num_missing_sends[active_credit] > 0){
      send_next_ru_request(active_credit);
      num_missing_sends[active_credit]--;
    } else if (fragment_received[active_credit] == ru_idx_to_lids.size()){
      fragment_received[active_credit] = 0;
      built_events++;
      emit(sig_built_events, my_rank);
      if (bu_idx_to_lids[0] == my_rank){
        std::cout << std::endl << "BU lid " << srcLid << " event completed " <<
            built_events << "/" << max_built << std::endl;
      }
      if(built_events < max_built || !is_max_built_active){
        send_em_request();
      } else {
        send_end();
      }
    }
  }
}

void EbApp::send_em_request()
{
  sendOutMsg(event_manager_lid, em_request_size, 0, 0, EM_REQUEST, 0,
       is_ideal_em, event_manager_lid-1, flit_accuracy);

  EV << "BU sending em request" << std::endl;
}

void EbApp::handle_end(IBDataMsg* p_msg)
{
  if (active_bu[bu_lid_to_idx[p_msg->getSrcLid()]]){
  	active_bu[bu_lid_to_idx[p_msg->getSrcLid()]] = false;
  	num_active_bu--;

  	std::cout << "EM: BU " << p_msg->getSrcLid() << " done" << std::endl;
  	if(num_active_bu == 0){
    	std::cout << "EM all BUs done exiting" << std::endl;
    	endSimulation();
  	}
  } else {
    std::cout << "EM duplicate end from " << p_msg->getSrcLid() << std::endl;
  }
}

void EbApp::send_end()
{
  std::cout << "BU lid " << srcLid << " events done sending END to th EM" << std::endl;

  sendOutMsg(event_manager_lid, 1, 0, 0, BU_END, 0, true, 0, flit_accuracy);
}

void EbApp::send_next_ru_request(int credit)
{
  // barrel shifter
  curr_send_idx[credit] = (curr_send_idx[credit] + 1)%ru_idx_to_lids.size();
  unsigned int msgDstLid = ru_idx_to_lids[curr_send_idx[credit]];

  EV << "BU " << srcLid << " sending request to RU " << msgDstLid << " for credit "
     << credit << std::endl;
  sendOutMsg(msgDstLid, ru_request_size, 0, 0, RU_REQUEST, credit,
       false, msgDstLid-1, flit_accuracy);

  if (pending_fragments[credit][curr_send_idx[credit]] == 0){
    pending_fragments[credit][curr_send_idx[credit]] = 2*number_of_rails;
  } else {
    opp_error("-E- overriding credit %d missing %d fragments from RU lid %d",
        credit, pending_fragments[credit][curr_send_idx[credit]],
        bu_idx_to_lids[curr_send_idx[credit]]);
  }
}

void EbApp::handleOut(IBAppMsg* msg)
{
    short app_msg_kind = (msg->getKind() & IB_APP_MASK);
    if (app_msg_kind == DATA){
      emit(sig_ru_sent_bytes, msg->getLenBytes());
    }
}

void EbApp::parse_config_file()
{
  std::string file_name(par("eb_config_file_name").stdstringValue());
  std::ifstream config_file(file_name);

  if (!config_file.is_open()){
    opp_error("-E- %s unable to open config file %s", getFullPath().c_str(),
        file_name.c_str());
  }

  config_file >> config_root;

  event_manager_lid = config_root["em_lid"].asInt();

  // setting up ru lids

  extract_vector(ru_lid_to_idx, ru_idx_to_lids, "ru_lids");

  extract_vector(bu_lid_to_idx, bu_idx_to_lids, "bu_lids");

  // setting up the bw limit
  if(fraction_distribution.size() == 0){
      std::string bw_limit_policy(par("bw_limit_policy").stdstringValue());
      if(bw_limit_policy == "none"){
          fraction_distribution.resize(ru_idx_to_lids.size(), 100);
      }else {
          // fraction_distribution.resize(ru_idx_to_lids.size());
          Json::Value bw_list = config_root["bw_list"];
          int num_ru = ru_idx_to_lids.size();
          int num_real_ru = 0;
          for (auto const & elem : bw_list){
              num_real_ru += elem[0].asInt();
          }
          int real_ru_curr_pos = 0;
          int real_ru_next_pos = 0;
          for (auto const & elem : bw_list){
              real_ru_next_pos = elem[0].asInt();
              fraction_distribution.insert(fraction_distribution.end(), round(elem[0].asInt()*(num_ru/(double)num_real_ru)), elem[1].asInt());
          }

          if(bw_limit_policy == "random"){
              unsigned seed = par("random_bw_policy_seed");
              shuffle(fraction_distribution.begin(), fraction_distribution.end(), std::default_random_engine(seed));
          }
      }
  }
}

void EbApp::extract_vector(std::vector<int>& lid_to_idx, std::vector<int>& idx_to_lid,
    std::string key)
{
  Json::Value tmp_value;

  tmp_value = config_root[key];

  if (!tmp_value.isArray()){
    opp_error("-E- %s bad config file %s is not a vector", getFullPath().c_str(),
        key.c_str());
  }

  int num_units = par("num_eb_nodes");
  if (num_units != -1){
    if (num_units < tmp_value.size()){
      tmp_value.resize(num_units);
    }
  }

  lid_to_idx.resize(tmp_value.size(), -1);
  for (auto const & elem : tmp_value){
    if (lid_to_idx.size() <= elem.asInt()){
      lid_to_idx.resize(elem.asInt()+1,-1);
    }
    lid_to_idx[elem.asInt()] = idx_to_lid.size();
    idx_to_lid.push_back(elem.asInt());
  }
}
