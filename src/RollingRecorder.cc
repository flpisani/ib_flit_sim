//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "RollingRecorder.h"

RollingRecorder::RollingRecorder() {
    _last_emission = 0;
    _normalizaion = 1;

    sum = 0;

}

RollingRecorder::~RollingRecorder() {
    // TODO Auto-generated destructor stub
}

void RollingRecorder::subscribedTo(cResultFilter* prev) {
    MyVectorRecorder::subscribedTo(prev);

    opp_string_map::iterator map_it;
    opp_string_map attributes = getStatisticAttributes();

    map_it = attributes.find("average_window");
    if(map_it != attributes.end()){
        _average_window = atof(map_it->second.c_str());
    }else{
        std::cerr << "RollingTimeAverage: average_window parameter must be specified" << std::endl;
        abort();
    }

    map_it = attributes.find("emission_frequency");
    if(map_it != attributes.end()){
        _emission_frequency = atof(map_it->second.c_str());
    }else{
        _emission_frequency = _average_window;
    }

    map_it = attributes.find("normalization");
    if(map_it != attributes.end()){
        _normalizaion = atof(map_it->second.c_str());
    }
}
