//     author  -   Flavio Pisani
//     email   -   flavio.pisani@cern.ch
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef ROLLINGRECORDER_H_
#define ROLLINGRECORDER_H_

#include "MyVectorRecorder.h"
#include <utility>
#include <list>

class RollingRecorder: public MyVectorRecorder {
protected:
    std::list <std::pair <simtime_t, double> > _values;
    simtime_t _average_window;
    simtime_t _emission_frequency;
    double _normalizaion;

    simtime_t _last_emission;

    double sum;

public:
    RollingRecorder();
    virtual ~RollingRecorder();

    virtual void subscribedTo(cResultFilter *prev);

};

#endif /* ROLLINGRECORDER_H_ */
