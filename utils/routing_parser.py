#     author  -   Flavio Pisani
#     email   -   flavio.pisani@cern.ch
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#

import re
import json
import sys
with open(sys.argv[1]) as f:
	lines = f.readlines()

with open(sys.argv[2]) as f:
	conversion_tree = json.load(f)
# you may also want to remove whitespace characters like `\n` at the end of each line
lines = [x.strip() for x in lines]

switch_routing = {}
switch_lid = -1;
for line in lines:
	switch_lid_match = re.match('LID\.+([0-9]+)', line)
	if switch_lid_match:
		switch_lid = int(switch_lid_match.group(1))

	if switch_lid != -1:
		try:
			switch_table = switch_routing[switch_lid]
		except KeyError:
			switch_routing[switch_lid] = {}
			switch_table = switch_routing[switch_lid]

		port_match = re.match('\s*([0-9]+)\s+([0-9]+)', line)
		if port_match:
			dst_lid = int(port_match.group(1))
			switch_port = int(port_match.group(2))
			if switch_port != 255:
				switch_table[dst_lid] = switch_port;

lid_idx_map = {}
for index, values in conversion_tree["node_lids"].iteritems():
	lid_idx_map[int(values["lid"])] = int(index)

# we dont't care for sw lids
switch_lid_idx_map = {}
for index, values in conversion_tree["switch_lids"].iteritems():
	switch_lid_idx_map[int(values["lid"])] = int(index)


for sw_lid, routing in switch_routing.iteritems():
# ignoring unknown values port 255 = drop
	port_lid_list = [255]*(len(lid_idx_map) + 1)
	for lid, port in routing.iteritems():
		try :
			port_lid_list[int(lid_idx_map[int(lid)])+1] = int(port) - 1
		except KeyError:
			pass

	print str(switch_lid_idx_map[sw_lid]) + ":", " ".join([str(x) for x in port_lid_list])
