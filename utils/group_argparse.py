#     author  -   Flavio Pisani
#     email   -   flavio.pisani@cern.ch
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#

import argparse

class GroupArgparser(argparse.ArgumentParser):
    """TODO add doc """
    def __init__(self):
        argparse.ArgumentParser.__init__(self)
        self.__group_param = {}
        self.__param_def = {}
        self.__options_groups = {}
        self.__args = 0

    def __add_groups_to_parser(self):

        for group in self.__group_param:
            self.__param_def.update(self.__group_param[group])

        for name, tmp_values in self.__param_def.items():
            short, help, conversion, action, default = tmp_values[:5]
            if (len(tmp_values) == 6):
                if(self.__options_groups.has_key(tmp_values[5]) == False):
                    self.__options_groups[tmp_values[5]] = self.add_mutually_exclusive_group()

                tmp_parser = self.__options_groups[tmp_values[5]]
            else:
                tmp_parser = self
            if action != None:
                tmp_parser.add_argument(short, "--" + name, help=help, action=action, default=default)
            else:
                tmp_parser.add_argument(short, "--" + name, help=help, type=conversion, default=default)

    def get_group_args(self, group_name):
        group_param = {}
        for name, tmp_values in self[group_name].items():
            short, help, conversion, action = tmp_values[:4]
            group_param[name] = getattr(self.__args, name)
        return group_param

    def add_group_param(self, group_name, param_def):
        self.__group_param[group_name] = param_def


    def remove_group_param(self, group_name):
        del self.__group_param[group_name]

    def get_group_param(self, group_name):
        return self.__group_param[group_name]

    def __getitem__(self, key):
        return self.get_group_param(key)

    def __delitem__(self, key):
        self.remove_group_param(key)

    def __setitem__(self, key, value):
        self.add_group_param(key, value)


    def parse_args(self):
        self.__add_groups_to_parser()
        self.__args = argparse.ArgumentParser.parse_args(self)
