#     author  -   Flavio Pisani
#     email   -   flavio.pisani@cern.ch
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#
import re
import json

class Topology_parser(object):
	def __init__(self):
		self.__switch_topology = []
		self.__node_list = []
		self.__lid_nodename_map = {}
		self.__lines = []
		self.__current_hca_name = ''
		# state 0 = init
		# state 1 = switch
		# state 2 = hca
		self.__state = 0
		self.__channel_name = "Channel"
		self.__node_component_name = "Node"
		self.__switch_component_name = "Switch"
		self.__node_instance_name = "node"
		self.__switch_instance_name = "switch"
		self.__switch_port_name = "port"
		self.__node_port_name = "port"

	def get_channel_name(self):
		return self.__channel_name

	def get_node_component_name(self):
		return self.__node_component_name

	def get_switch_component_name(self):
		return self.__switch_component_name

	def get_node_instance_name(self):
		return self.__node_instance_name

	def get_switch_instance_name(self):
		return self.__switch_instance_name

	def set_channel_name(self, value):
		self.__channel_name = value

	def set_node_component_name(self, value):
		self.__node_component_name  = value

	def set_switch_component_name(self, value):
		self.__switch_component_name  = value

	def set_node_instance_name(self, value):
		self.__node_instance_name = value

	def set_switch_instance_name(self, value):
		self.__switch_instance_name = value

	def get_switch_topology(self):
		return self.__switch_topology

	def get_node_list(self):
		return self.__node_list

	def get_lid_nodename_map(self):
		return self.__lid_nodename_map

	def read_lines(self, file_name):
		with open(file_name) as f:
			self.__lines = f.readlines()
		self.__lines = [x.strip() for x in self.__lines]

	def set_lines(self, lines):
		self.__lines = lines

	def __match_switch(self, line):
		return re.match('^Switch.+lid ([0-9]+)', line)

	def __match_switch_port(self, line):
		return re.match('^\[([0-9]+)\].+\[([0-9]+)\].+lid\s+([0-9]+)', line)

	def __match_node(self, line):
		return re.match('^Ca.+\"node([0-9]+)', line)

	def __match_node_port(self, line):
		return re.match('^\[([0-9]+)\].+#\s+lid\s+([0-9]+)', line)

	def __parse_switch(self, match):
		switch_lid = int(match.group(1))
		self.__switch_topology.append((switch_lid,[]))
		self.__lid_nodename_map[switch_lid] = self.__switch_instance_name + '[' + str(len(self.__switch_topology) - 1) + ']'

	def __parse_switch_port(self, match):
		self.__switch_topology[-1][1].append([int(x) for x in match.groups()])

	def __parse_node(self, match):
		self.__current_hca_name = int(match.group(1))

	def __parse_node_port(self, match):
		node_lid = int(match.group(2))
		self.__node_list.append((self.__current_hca_name,node_lid))
		self.__lid_nodename_map[node_lid] = self.__node_instance_name + '[' + str(len(self.__node_list) - 1) + ']'

	def parse_topology(self):
		state = 0

		for line in self.__lines:
			if (state == 0):
				match = self.__match_switch(line)
				if match:
					state = 1
					self.__parse_switch(match)
				else:
					match = self.__match_node(line)
					if match:
						state = 2
						self.__parse_node(match)
			elif (state == 1):
				match = self.__match_switch_port(line)
				if match:
					self.__parse_switch_port(match)
				else:
					state = 0
			elif (state == 2):
				match = self.__match_node_port(line)
				if match:
					self.__parse_node_port(match)
				else:
					state = 0

	def print_ned(self):
		print "submodules:"
		print "\t" + self.__switch_instance_name + "[" + str(len(self.__switch_topology)) + "] : ", self.__switch_component_name, "{}"

		print "\t" + self.__node_instance_name + "[" + str(len(self.__node_list)) + "] :", self.__node_component_name, "{}\n"

		print "connections allowunconnected:"
		for src_lid, connections in self.__switch_topology:
			src_node = self.__lid_nodename_map[src_lid]
			for src_port, dst_port, dst_lid in connections:
				try:
					dst_node = self.__lid_nodename_map[dst_lid]
					if re.match(self.__switch_instance_name, dst_node) and re.match(self.__switch_instance_name, src_node):
						if dst_lid < src_lid:
							print "\t" + src_node + "." + self.__switch_port_name  + "[" + \
							 str(src_port - 1) + "] <-->", self.__channel_name, "<-->",\
							 dst_node + "." + self.__switch_port_name + "[" + str(dst_port - 1) + "];"
					elif re.match(self.__node_instance_name, dst_node):
						print "\t" + src_node + "." + self.__switch_port_name  + "[" + \
						 str(src_port - 1) + "] <-->", self.__channel_name, "<-->",\
						 dst_node + "." + self.__node_port_name + ";"
					else :
					 print "\t" + src_node + "." + self.__node_port_name, "<-->",\
					  self.__channel_name, "<-->", dst_node + "." + \
					  self.__switch_port_name + "[" + str(dst_port - 1) + "];"
				except KeyError:
					pass

	def generate_json(self):
		json_root = {}

		node_dict = {}
		for node in self.__node_list:
			tmp_dict = {}
			tmp_dict["host_name"] = node[0]
			tmp_dict["lid"] = node[1]
			node_dict[self.__node_list.index(node)] = tmp_dict

		json_root["node_lids"] = node_dict

		switch_dict = {}
		for switch in self.__switch_topology:
			tmp_dict = {}
			tmp_dict["lid"] = switch[0]
			switch_dict[str(self.__switch_topology.index(switch))] = tmp_dict

		json_root["switch_lids"] = switch_dict

		return json_root

	def print_json(self):
		print json.dumps(self.generate_json(), sort_keys=True, indent=4, separators=(',', ': '))

if __name__ == '__main__':
	import group_argparse

	main_param_def = {"input_file" : ("-i", "input file", str, None, None),
					  "print_json" : ("-j", "generate and print the json", None, "store_true", False),
					  "print_ned" : ("-n", "generate and print the NED", None, "store_true", False),
					  "channel_name" : ("-c", "set the channel name for the NED", str, None, "Delay_channel"),
					  "node_component_name" : ("-O", "set the node component name for the NED", str, None, "Node"),
					  "node_instance_name" : ("-o", "set the node instance name for the NED", str, None, "node"),
					  "switch_component_name" : ("-S", "set the switch component name for the NED", str, None, "Switch"),
					  "switch_instance_name" : ("-s", "set the switch instance name for the NED", str, None, "switch"),
					  "node_port_name" : ("-p", "set the node port name for the NED", str, None, "port"),
					  "switch_port_name" : ("-P", "set the switch port name for the NED", str, None, "port"),
	}

	argument_parser = group_argparse.GroupArgparser()

	argument_parser["main"] = main_param_def

	argument_parser.parse_args()

	main_param = argument_parser.get_group_args("main")

	parser = Topology_parser()

	parser.set_channel_name(main_param["channel_name"])
	parser.set_node_component_name(main_param["node_component_name"])
	parser.set_node_instance_name(main_param["node_instance_name"])
	parser.set_switch_component_name(main_param["switch_component_name"])
	parser.set_switch_instance_name(main_param["switch_instance_name"])

	parser.read_lines(main_param["input_file"])

	parser.parse_topology()

	if(main_param["print_json"]):
		parser.print_json()

	if(main_param["print_ned"]):
		parser.print_ned()
