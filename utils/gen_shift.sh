#!/bin/bash
#     author  -   Flavio Pisani
#     email   -   flavio.pisani@cern.ch
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#

num_nodes=$1
shift
num_active_nodes=$1
shift

for k in `seq 0 $(( num_active_nodes - 1 ))`
do
	if (( k == 0 ))
	then
		echo -en "$k: "
	else
		echo -en "\\n$k: "
	fi
	for j in `seq 0 $(( num_active_nodes - 1 ))`
	do
		echo -n "$(( (j+k)%(num_active_nodes) + 1 )) "
	done
done

for k in `seq $num_active_nodes $(( num_nodes - 1 ))`
do
	echo -en "\\n$k: 0"
done

echo ""
