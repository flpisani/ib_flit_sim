#     author  -   Flavio Pisani
#     email   -   flavio.pisani@cern.ch
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#
import re
import json
import sys
import group_argparse
import random

main_param_def = {"num_nodes" : ("-n", "number of nodes", int, None, 0),
						"num_active_nodes" : ("-a", "number of active nodes", int, None, 0),
						"host_idx_file" : ("-f", "host index mapping json file", str, None, None),
						"random_ordering" : ("-r", "order the nodes randomly", None, "store_true", False),
					  }
argument_parser = group_argparse.GroupArgparser()

argument_parser["main"] = main_param_def

argument_parser.parse_args()

main_param = argument_parser.get_group_args("main")

num_nodes = main_param["num_nodes"]
num_active_nodes = main_param["num_active_nodes"]

host_idx_file = main_param["host_idx_file"]
random_ordering = main_param["random_ordering"]

if ( host_idx_file != None ):
	with open(host_idx_file) as f:
		host_idx = [ (int(value["host_name"]), int(idx)+1) for idx, value in json.load(f)["node_lids"].iteritems()]
else:
	host_idx = [(i,i+1) for i in range(0,num_nodes)]

if ( random_ordering ):
	random.shuffle(host_idx)
else :
	host_idx.sort()

idx_list = zip(*host_idx)[1]

for k in range(0, num_active_nodes):
	line = str(k) + ": "
	for j in range(0, num_active_nodes):
		line += str(idx_list[(j+k)%(num_active_nodes)]) + " "
	print line


for k in range(num_active_nodes,num_nodes):
	print str(k) + ":" , 0

#for k in `seq 0 $(( num_active_nodes - 1 ))`
#do
#if (( k == 0 ))
#then
#echo -en "$k: "
#else
#echo -en "\\n$k: "
#fi
#for j in `seq 0 $(( num_active_nodes - 1 ))`
#do
#echo -n "$(( (j+k)%(num_active_nodes) + 1 )) "
#done
#done
#
#for k in `seq $num_active_nodes $(( num_nodes - 1 ))`
#do
#echo -en "\\n$k: 0"
#done
#
#echo ""
