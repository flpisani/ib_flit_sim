switch_radix = 6
nodes_per_switch = switch_radix/2
num_nodes = nodes_per_switch*(nodes_per_switch+1)
num_leaf_switches = switch_radix/2+1;
num_spine_switches = nodes_per_switch;

for sw_idx in range(0,num_leaf_switches):
	line = str(sw_idx) + ": 255"
	for host_lid in range(0,num_nodes):
		if (int(host_lid/(nodes_per_switch)) == sw_idx):
			line += " " + str(host_lid%(nodes_per_switch))
		else :
			line += " " + str(nodes_per_switch + host_lid%num_spine_switches)

	print line

for sw_idx in range(num_leaf_switches,num_leaf_switches + num_spine_switches):
	line = str(sw_idx) + ": 255"
	for host_lid in range(0,num_nodes):
		line += " " + str(host_lid/nodes_per_switch)

	print line

