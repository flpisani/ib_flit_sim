import sys

switch_radix = int(sys.argv[1])
num_spine = switch_radix/2
nodes_per_leaf = switch_radix/2
num_leaf = switch_radix
num_nodes = num_leaf*nodes_per_leaf

for sw_idx in range(0,num_leaf):
	line = str(sw_idx) + ": 255"
	for host_lid in range(0,num_nodes):
		if (int(host_lid/nodes_per_leaf) == sw_idx):
			line += " " + str(host_lid%nodes_per_leaf)
		else :
			line += " " + str(nodes_per_leaf + host_lid%num_spine)

	print line

for sw_idx in range(num_leaf, num_leaf + num_spine):
	line = str(sw_idx) + ": 255"
	for host_lid in range(0,num_nodes):
		line += " " + str(host_lid/nodes_per_leaf)

	print line

